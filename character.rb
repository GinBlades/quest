require './game_object'
class Character < GameObject
  attr_accessor :inventory, :attack_strength, :defense_strength, :health
  attr_reader :name
  
  def initialize(name)
    super
    @object_type = 'Character'
    @name = name
    @attack_strength = 10
    @defense_strength = 5
    @health = 20
    @inventory = ['sword', 'shield']
  end
  
  def add_item(item)
    @inventory << item
  end
  
  def list_inventory
    puts @inventory.join(', ')
  end

  def kill(target)
    target.health -= @attack_strength - target.defense_strength
    puts "AS #{@attack_strength} - DS #{target.defense_strength} : Target HP = #{target.health}"
  end
end