require 'singleton'

class GameObjectList
  include Singleton

  attr_accessor :objects

  def initialize
    @objects = []
  end

  def add *objects
    objects.each do |object|
      unless @objects.include? object
        @objects << object
      end
    end
  end
end