require './game_object_list'
class GameObject
  attr_reader :id, :object_type

  def initialize *args
    @id = rand(100000)
    @object_type = 'Game Object'
    GameObjectList.instance.add(self)
  end
end