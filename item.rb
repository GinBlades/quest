require './game_object'
class Item < GameObject
  attr_accessor :name

  def initialize(name)
    @name = name
    @object_type = 'Item'
  end
end