require "readline"
require './game_object'
require './game_object_list'
require "./areas/area_1"
require "./player"

class Main
  def initialize
    @game_object_list = GameObjectList.instance
    @area = Area1.instance
    @player = Player.new('Player')
    @end_game = false
  end
  
  def process(input)
    case input
      when 'north', 'n'
        @area.move(1)
      when 'south', 's'
        @area.move(2)
      when 'east', 'e'
        @area.move(3)
      when 'west', 'w'
        @area.move(4)
      when 'look', 'l'
        @area.full_description
      when 'inventory', 'inv'
        @player.list_inventory
      when /\Akill ([\w\s]+)/
        @player.kill(@area.available_enemies[0])
      when /\Aget ([\w\s]+)/
        if @area.available_items.include? $1
          @player.add_item($1)
          @area.remove_items($1)
          puts "You pick up #{$1}"
        end
      when 'exit', 'quit', 'q', 'x'
        @end_game = true
      else
        puts 'Not a valid command'
    end  
  end
  
  def run
    all_objs = []
    @game_object_list.objects.each do |obj|
      all_objs << [obj.id, obj.object_type]
    end
    print all_objs
    puts all_objs.count
    @area.full_description
    while buf = Readline.readline("> ", true)
      process buf
      break if @end_game
    end    
  end
end

main = Main.new
main.run

