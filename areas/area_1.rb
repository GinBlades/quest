require './areas/area'
require 'singleton'

class Area1 < Area
  include Singleton
  
  def grid
    [
      [1,  0,  2,  14, 15, 16],
      [3,  4,  5,  0,  0,  17],
      [6,  0,  7,  0,  19, 18],
      [8,  9,  10, 0,  0,  20],
      [11, 12, 13, 23, 22, 21]
    ]
  end

  def enemies
    {
      kobold: Enemy.new('a kobold'),
      goblin: Enemy.new('a goblin')
    }
  end
  
  def room_descriptions
    rooms = Hash.new do |hash, key|
      hash[key] = {
        supplies: [],
        enemies: [],
        description: 'This is a very dark room.'
      }
    end
    rooms[1] = {
        supplies: ['a blue potion', 'a white feather'],
        description: 'This is the first room.'}
    rooms[2] = {
        supplies: ['a smooth stone'],
        description: 'This is room 2.'}
    rooms[3] = {
        description: 'This is the second room you can get to.'}
    rooms[4] = {
        enemies: [enemies[:kobold]],
        description: 'This is room 4.'}
    rooms[13] = {
        enemies: [enemies[:goblin]],
        description: 'This is room 13.'}
    rooms
  end
end