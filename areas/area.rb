require "./areas/room"
require "./enemy"

class Area
  def initialize
    @x = 0
    @y = 0
    @area_rooms = create_rooms
  end
  
  def grid
    [[]]
  end

  def current_location
    grid[@y][@x]
  end

  def can_move?(direction, value)
    max_x = grid[0].size
    max_y = grid.size
    if value < 0
      return false
    end
    if direction == 'y'
      if value >= max_y || grid[value][@x] == 0
        return false
      end
    elsif direction == 'x'
      if value >= max_x || grid[@y][value] == 0
        return false
      end
    end
    true
  end
  
  def available_exits
    exits = []
    if can_move? 'y', @y - 1
      exits << 'north'
    end
    if can_move? 'y', @y + 1
      exits << 'south'
    end
    if can_move? 'x', @x + 1
      exits << 'east'
    end
    if can_move? 'x', @x - 1
      exits << 'west'
    end
    exits.join(', ')
  end
  
  def available_items
    @area_rooms[current_location].items
  end

  def available_enemies
    @area_rooms[current_location].enemies
  end
  
  def remove_items(item)
    items = @area_rooms[current_location].items
    hash = Hash[items.map.with_index.to_a]
    index = hash[item]
    @area_rooms[current_location].items.delete_at(index)
  end
  
  def move(input)
    case input
      when 1
        @y -= 1 if can_move? 'y', @y - 1
      when 2
        @y += 1 if can_move? 'y', @y + 1
      when 3
        @x += 1 if can_move? 'x', @x + 1
      when 4
        @x -= 1 if can_move? 'x', @x - 1
    end
    full_description
  end
  
  def room_descriptions
    rooms = Hash.new do |hash, key|
      hash[key] = {
        supplies: [],
        enemies: [],
        description: 'This is a very dark room.'
      }
    end
    rooms
  end
  
  def create_rooms
    hash = Hash.new
    total_rooms = 0
    grid.each do |row|
      row.each do |room|
        if room > 0
          hash[room] = Room.new({
            id: room,
            description: room_descriptions[room][:description],
            items: room_descriptions[room][:supplies],
            enemies: room_descriptions[room][:enemies]
          })
        end
      end
    end
    hash
  end
    
  def full_description
    puts @area_rooms[current_location].description
    unless @area_rooms[current_location].items.empty?
      puts 'You also see: ' + @area_rooms[current_location].items.join(', ')
    end
    unless @area_rooms[current_location].enemies.empty?
      puts 'Also here: ' + @area_rooms[current_location].enemies.map{|n| n.name}.join(', ')
    end    
    puts "Available Exits: " + available_exits
  end

end