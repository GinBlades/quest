class Room < GameObject
  attr_reader :id, :description #, :exits
  attr_accessor :items, :enemies
  
  def initialize(hash)
    @object_type = 'Room'
    @id = hash[:id]
    @description = hash[:description] || 'This is a very dark room.'
#     @exits = hash[:exits]
    @items = hash[:items] || []
    @enemies = hash[:enemies] || []
  end
end